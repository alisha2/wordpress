<?php
/*
Plugin Name: goarch
Plugin URI:
Description: goarch
Version: 1.0.7
Author: Victor
Author URI:
License:
*/






require plugin_dir_path(__FILE__) . '/shortcodes/shortcodes.php';
require plugin_dir_path(__FILE__) . '/VC_custum-data.php';


require plugin_dir_path(__FILE__) . '/import_demo.php';
require plugin_dir_path(__FILE__) . '/css_generator.php';
require plugin_dir_path(__FILE__) . '/function.php';
require plugin_dir_path(__FILE__) . '/metabox.php';
require plugin_dir_path(__FILE__) . '/widgets.php';
require plugin_dir_path(__FILE__) . '/categoris-image.php';
require plugin_dir_path(__FILE__) . '/ot_demo_function.php';
require plugin_dir_path(__FILE__) . '/custom-style.php';
require plugin_dir_path(__FILE__) . '/contact_form.php';



function goarch_get_tememe_color_p(){
    $type = get_theme_mod( 'goarch_performans_style', 'dark' );
    if(isset($_GET['style'])){
        $arr= array('dark','light');
        if(in_array(sanitize_text_field($_GET['style']),$arr)){
            $type = sanitize_text_field($_GET['style']);
        }
    }
    return $type;
}

/**
 *Create the desired tables for theme
 */


add_action('init', 'goarch_projects_init');
/**
 * great projects custom type post
 */
function goarch_projects_init()
{
    $args = array(
        'label' => esc_html__('Projects', 'goarch'),
        'labels' => array(
            'edit_item' => esc_html__('Edit', 'goarch'),
            'add_new_item' => esc_html__('Add', 'goarch'),
            'view_item' => esc_html__('View', 'goarch'),
        ),
        'singular_label' => esc_html__('Event', 'goarch'),
        'has_archive' => true,
        'public' => true,
        'show_ui' => true,
        '_builtin' => false,
        '_edit_link' => 'post.php?post=%d',
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-groups'
    );

    $args['label'] = esc_html__('Projects', 'goarch');
    $args['singular_label'] = esc_html__('Item', 'goarch');
    register_post_type('projects', $args);
    register_taxonomy(
        'projects_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'projects',         //post type name
        array(
            'hierarchical' => true,
            'label' => esc_html__('Category', 'goarch'),  //Display name
            'query_var' => true,
            'rewrite' => array('slug' => 'project')

        )
    );

}



add_shortcode('goarch_social_links', 'goarch_social_links_function');


function goarch_social_links_function($atts)
{
    $atts = shortcode_atts(
        array(
            'url' => '#',
            'class' => '',
        ), $atts
    );
    ob_start();
    ?>

<li>
    <a class="<?php echo wp_kses_post($atts['class']) ?>"   href="<?php echo esc_url($atts['url']); ?>"></a></li>
    <?php
    return ob_get_clean();
}


/**
 * image meta box
 */



add_action( 'wp_head', 'goarch_buffer_start', 999 );
add_action( 'wp_footer', 'goarch_buffer_end', 999 );
 

/**
 *buffer start
 */
function goarch_buffer_start() {
	ob_start( "goarch_replace_edited_section" );
	?>
   


	<?php
}

function goarch_buffer_end() {
	ob_end_flush();

}




/*
 *
 */
function goarch_replace_edited_section( $buffer ) {
	$buffer = str_replace( '``',
		'', $buffer );

	return $buffer;
}


