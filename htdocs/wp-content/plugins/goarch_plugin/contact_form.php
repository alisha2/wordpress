<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.11.2016
 * Time: 17:40
 */

function goarch_mail_send() {

	if ( isset( $_POST['email'] ) && isset( $_POST['name'] ) && isset( $_POST['message'] ) ) {

		// detect & prevent header injections
		$test = "/(content-type|bcc:|cc:|to:)/i";
		foreach ( $_POST as $key => $val ) {
			if ( preg_match( $test, $val ) ) {
				exit;
			}
		}

		$to = esc_html( get_option( 'admin_email' ) );
		if ( strlen( get_theme_mod( 'goarch_mail_email' ) ) > 3 && is_email(get_theme_mod( 'goarch_mail_email' ) ) ) {
			$to = esc_html( get_theme_mod( 'goarch_mail_email' ) );
		}


		$email = sanitize_text_field( $_POST['email'] );
		$company = '';
		$phone = '';
		$name = wp_kses_post($_POST['name']);
		if ( isset( $_POST['phone'] ) ) {
			$phone = "\n\r <br>" . esc_html__( 'Phone number: ', 'goarch' ) . sanitize_text_field( $_POST['phone'] ) . ' ' . "\n\r <br>";
		}
		if ( isset( $_POST['company'] ) ) {
			$company = "\n\r <br>" . esc_html__( 'Company: ', 'goarch' ) . sanitize_text_field( $_POST['company'] ) . ' ' . "\n\r <br>";
		}
		$headers[] = "From:  $name {$email} < $to >";
		$headers[] = 'content-type: text/html';

		$send = wp_mail( $to, sanitize_text_field( $_POST['name'] ), wp_kses_post( $_POST['message'] ) . '<br>' .
		                                                             $_POST['name']  .' ' .   $company . $phone, $headers );
		/*$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$send = mail( $to, sanitize_text_field( $_POST['name'] ), wp_kses_post( $_POST['message'] ) . '<br>' .
		                                                          $_POST['name']  .' ' .   $company . $phone, $headers );
*/

		if ( $send ) {
			echo 1;
		} else {
			echo 0;

		}


	}
	wp_die();
	exit;
}

add_action( 'wp_ajax_goarch_mail_send', 'goarch_mail_send' ); // for logged in user
add_action( 'wp_ajax_nopriv_goarch_mail_send', 'goarch_mail_send' ); // if user not logged in
