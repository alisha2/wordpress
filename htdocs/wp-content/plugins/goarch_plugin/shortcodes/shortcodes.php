<?php


/*
* goarch_main_slider
*/


add_shortcode( 'goarch_main_slider', 'goarch_main_slider_func' );
function goarch_main_slider_func( $atts, $content ) {
	ob_start();
	$content = !empty( $content ) ? $content : "";

	$atts = shortcode_atts(
		array(
			'alias_slider' => '',
			'img' => '',

		), $atts
	);


	extract( $atts );


	?>


    <!-- Home -->

    <main class="main ">

        <div class="arrow-left"></div>
        <div class="arrow-right"></div>

        <!-- Start goarchlution slider -->

        <div class="rev_slider_wrapper">
            <div id="rev_slider" class="rev_slider fullscreenbanner">

                <!-- Main image-->
				<?php if ( isset( $img_src[0] ) ) {
					$img = wp_get_attachment_image_src( $img_src, 'full' );

					?>

                    <img src="<?php echo esc_url( $img ); ?>" alt=""
                         data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                         class="rev-slidebg">
					<?php
				} ?>

				<?php

				if ( $alias_slider != '' ) {
					echo do_shortcode( '[rev_slider alias="' . $alias_slider . '"][/rev_slider]' );
				} ?>


            </div>
        </div>
    </main>
	<?php
	return ob_get_clean();
}


/*
 * goarch_about
 */
add_shortcode( 'goarch_about', 'goarch_about_func' );
function goarch_about_func( $atts, $content ) {
	ob_start();
	$old_flag = false;

	$content = !empty( $content ) ? $content : esc_html__( 'For each project we establish relationships with partners who we know will help us create added value for your project. As well as bringing together the public and private sectors, we make sector-overarching links to gather knowledge and to learn from each other. The way we undertake projects is based on permanently applying values that reinforce each other: socio-cultural value, experiental value, building-technical value and economical value.', 'goarch' );

	$atts = shortcode_atts(
		array(
			'section_id' => '',
			'title' => esc_html__( 'About', 'goarch' ),
			't_p' => esc_html__( 'go.arch', 'goarch' ),
			'col_title' => esc_html__( 'we turn ideas into works of art', 'goarch' ),
			'col_primary' => esc_html__( '.', 'goarch' ),
			'col_about_info' => esc_html__( 'For each project we establish relationships with partners who we know will help us create added value for your project. As well as bringing together the public and private sectors, we make sector-overarching links to gather knowledge and to learn from each other. The way we undertake projects is based on permanently applying values that reinforce each other: socio-cultural value, experiental value, building-technical value and economical value.', 'goarch' ),
			'img' => '',
			'spec' => '',
			'items' => '',
			'css' => '',
			'spec_primary' => esc_html__( ':', 'goarch' ),

		), $atts
	);


	extract( $atts );

	$tems_v = vc_param_group_parse_atts( $atts['items'] );
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), '', $atts );
	$img_arr = wp_get_attachment_image_src( $img, 'full' );
	?>


    <!-- About  -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="about section <?php echo esc_attr( $css_class ); ?>">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title"><?php
					echo wp_kses_post( $title ); ?> <span class="text-primary"><?php
						echo wp_kses_post( $t_p ); ?></span></h2>
                <strong class="fade-title-left"><?php
					echo wp_kses_post( $title ); ?></strong>
            </header>
            <div class="section-content">
                <div class="row-base row">
                    <div class="col-base col-sm-6 col-md-4">
                        <h3 class="col-about-title"><?php
							echo wp_kses_post( $col_title ); ?><span
                                    class="text-primary"><?php
								echo wp_kses_post( $col_primary ); ?></span></h3>
                        <div class="col-about-info">
                            <p><?php

								echo wp_kses_post( $col_about_info ); ?></p>
                        </div>
                    </div>
                    <div class="col-base col-about-spec col-sm-6 col-md-4">
                        <h3 class="col-about-title"><?php
							echo wp_kses_post( $spec ); ?>
                            <span class="text-primary"><?php if ( isset( $spec_primary{1} ) ) {

									echo wp_kses_post( $spec_primary );
								} ?></span></h3>
						<?php

						if ( isset( $tems_v{0} ) ) {

							foreach ( $tems_v as $item ) {
								$img = wp_get_attachment_image_src( $item ['images'], 'full' );
								?>
                                <div class="service-item">
									<?php if ( isset( $item['url_l'] ) ) { ?>
                                        <a href="<?php echo esc_url( $item['url_l'] ); ?>">
                                            <img alt="" width="46" src="<?php if ( isset( $img[0]{1} ) ) {
												echo esc_url( $img[0] );
											} ?>">

                                        </a>
									<?php } else { ?>
                                        <img alt="" width="46" src="<?php if ( isset( $img[0]{1} ) ) {
											echo esc_url( $img[0] );
										} ?>">
									<?php } ?>
                                    <h4><?php echo wp_kses_post( $item['i_title'] ); ?></h4>
                                </div>
							<?php }
						}
						?>
                    </div>
                    <div class="clearfix visible-sm"></div>
                    <div class="col-base col-about-img col-sm-6 col-md-4">
                        <img alt="" class="img-responsive" src="<?php echo esc_url( $img_arr[0] ); ?>">
                    </div>
                </div>
            </div>
        </div>
    </section>
	<?php
	return ob_get_clean();
}

add_shortcode( 'goarch_about_new', 'goarch_about_new_func' );
function goarch_about_new_func( $atts, $content ) {
	ob_start();
	$old_flag = false;

	$content = !empty( $content ) ? $content : "";

	$atts = shortcode_atts(
		array(
			'section_id' => '',
			'title' => esc_html__( 'About', 'goarch' ),
			't_p' => esc_html__( 'go.arch', 'goarch' ),
			'col_title' => esc_html__( 'we turn ideas into works of art', 'goarch' ),
			'col_primary' => esc_html__( '.', 'goarch' ),
			'col_about_info' => esc_html__( 'For each project we establish relationships with partners who we know will help us create added value for your project. As well as bringing together the public and private sectors, we make sector-overarching links to gather knowledge and to learn from each other. The way we undertake projects is based on permanently applying values that reinforce each other: socio-cultural value, experiental value, building-technical value and economical value.', 'goarch' ),
			'img' => '',
			'spec' => '',
			'items' => '',
			'css' => '',
			'spec_primary' => esc_html__( ':', 'goarch' ),

		), $atts
	);


	extract( $atts );

	$tems_v = vc_param_group_parse_atts( $atts['items'] );
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), '', $atts );
	$img_arr = wp_get_attachment_image_src( $img, 'full' );
	?>


    <!-- About  -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="about section <?php echo esc_attr( $css_class ); ?>">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title"><?php
					echo wp_kses_post( $title ); ?> <span class="text-primary"><?php
						echo wp_kses_post( $t_p ); ?></span></h2>
                <strong class="fade-title-left"><?php
					echo wp_kses_post( $title ); ?></strong>
            </header>
            <div class="section-content">
                <div class="row-base row">
                    <div class="col-base col-sm-6 col-md-4">
                        <h3 class="col-about-title"><?php
							echo wp_kses_post( $col_title ); ?><span
                                    class="text-primary"><?php
								echo wp_kses_post( $col_primary ); ?></span></h3>
                        <div class="col-about-info">
							<?php echo wp_kses_post( $content ); ?>

                        </div>
                    </div>
                    <div class="col-base col-about-spec col-sm-6 col-md-4">
                        <h3 class="col-about-title"><?php
							echo wp_kses_post( $spec ); ?>
                            <span class="text-primary"><?php if ( isset( $spec_primary{1} ) ) {

									echo wp_kses_post( $spec_primary );
								} ?></span></h3>
						<?php

						if ( isset( $tems_v{0} ) ) {

							foreach ( $tems_v as $item ) {
								$img = wp_get_attachment_image_src( $item ['images'], 'full' );
								?>
                                <div class="service-item">
									<?php if ( isset( $item['url_l'] ) ) { ?>
                                        <a href="<?php echo esc_url( $item['url_l'] ); ?>">
                                            <img alt="" width="46" src="<?php if ( isset( $img[0]{1} ) ) {
												echo esc_url( $img[0] );
											} ?>">

                                        </a>
									<?php } else { ?>
                                        <img alt="" width="46" src="<?php if ( isset( $img[0]{1} ) ) {
											echo esc_url( $img[0] );
										} ?>">
									<?php } ?>
                                    <h4><?php echo wp_kses_post( $item['i_title'] ); ?></h4>
                                </div>
							<?php }
						}
						?>
                    </div>
                    <div class="clearfix visible-sm"></div>
                    <div class="col-base col-about-img col-sm-6 col-md-4">
                        <img alt="" class="img-responsive" src="<?php echo esc_url( $img_arr[0] ); ?>">
                    </div>
                </div>
            </div>
        </div>
    </section>
	<?php
	return ob_get_clean();
}

/*
 * goarch_project
 */
add_shortcode( 'goarch_project', 'goarch_project_func' );
function goarch_project_func( $atts, $content ) {
	ob_start();
	$content = !empty( $content ) ? $content : "";

	$atts = shortcode_atts(
		array(
			'css' => '',
			'heading' => '',
			'posts' => esc_html__( '8 ', 'goarch' ),
			'order' => 'DESC',
			'orderby' => 'date',
			'projects_category' => 'all',
			'title' => esc_html__( 'OUR ', 'goarch' ),
			't_p' => esc_html__( 'PROJECTS ', 'goarch' ),
			'post_id' => '',
			'section_id' => '',
			'type_l' => '1',
			'img_type' => '1'


		), $atts
	);


	extract( $atts );


	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), '', $atts );

	$myarray = array();

	$myarray = explode( ',', $post_id );


	$args = array(
		'posts_per_page' => $posts,
		'order' => $order,
		'post_status' => 'publish',
		'post_type' => 'projects',
		'meta_query' => array(
			array(
				'key' => '_thumbnail_id'
			)
		)


	);

	if ( $projects_category != 'all' ) {
		$str = $projects_category;
		$arr = explode( ',', $str );

		$args['tax_query'] = array(
			array(
				'taxonomy' => 'projects_categories',
				'terms' => $arr,
				'field' => 'slug'
			)
		);

	}


	if ( isset( $post_id{1} ) ) {
		$args['post__in'] = $myarray;
		$args['orderby'] = 'post__in';

	} else {
		$args['orderby'] = $orderby;
	}


	?>


    <!-- Projects -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="projects section <?php echo esc_attr( $css_class ); ?>">
        <div class="container">
            <h2 class="section-title"><?php if ( isset( $title{0} ) ) {
					echo wp_kses_post( $title );
				} ?> <span
                        class="text-primary"><?php if ( isset( $t_p{0} ) ) {
						echo wp_kses_post( $t_p );
					} ?></span></h2>
        </div>
        <div class="section-content">
            <div class="projects-carousel js-projects-carousel js-projects-gallery">

				<?php

				$goarch_projects_query = new WP_Query( $args );
				$j = 1;
				if ( $goarch_projects_query->have_posts() ):
					while ( $goarch_projects_query->have_posts() ) {
						$goarch_projects_query->the_post();
						$class = '';
						if ( $j % 2 == 0 ) {
							$class = ' project-light';
						}
						$j ++;
						?>
                        <div class="project <?php echo esc_attr( $class ); ?>">
							<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                            <a
								<?php if ( $type_l == 2 ) { ?>
                                    class="link"
								<?php } ?>
                                    href="<?php if ( $type_l == 2 ) {
										the_permalink();
									} else {
										echo esc_url( $image_url[0] );
									} ?>" title=" <?php echo the_title(); ?>">

                                <figure>
									<?php if ( $img_type == 1 ) { ?>
                                        <img src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ); ?>">
									<?php } else {
										the_post_thumbnail( 'goarch-image-480x880-croped' );
									} ?>


                                    <figcaption>
                                        <h3 class="project-title">
											<?php echo the_title(); ?>
                                        </h3>
                                        <h4 class="project-category">
                                        <?php
										$terms = get_the_terms( get_the_ID(), 'projects_categories' );
										if ( $terms ) {
											foreach ( $terms as $term ) {
												?>
                                              	<?php echo wp_kses_post( $term->name ); ?>

                                               <br>
												<?php
											}
										}
										?>  </h4>
                                        <div class="project-zoom"></div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
						<?php

					}
					wp_reset_postdata();
				endif; ?>

            </div>
        </div>
    </section>

	<?php
	return ob_get_clean();
}

/*
 * Experience
 */
add_shortcode( 'goarch_experience', 'goarch_experience_func' );
function goarch_experience_func( $atts, $content ) {
	ob_start();
	$content = !empty( $content ) ? $content : "";

	$atts = shortcode_atts(
		array(
			'css' => '',
			'heading' => esc_html__( 'Years of successful work', 'goarch' ),
			'heading_2' => esc_html__( 'in the market', 'goarch' ),
			'content2' => esc_html__( '12', 'goarch' ),
			'img' => '',
			'section_id' => '',

		), $atts
	);


	extract( $atts );


	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), '', $atts );

	?>


    <!-- Experience -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="experience section <?php echo esc_attr( $css_class ); ?>">
        <div class="container">
			<?php if ( isset( $img[0] ) ) {
				$img_src = wp_get_attachment_image_src( $img, 'full' );


			} ?>
            <div class="text-parallax" data-stellar-background-ratio="0.85" <?php if ( isset( $img_src{0} ) ) { ?>
                 style="background-image: url('<?php echo esc_url( $img_src[0] ); ?>');">
				<?php

				} ?>
                <div class="text-parallax-content"><?php if ( isset( $content2{0} ) ) {
						echo wp_kses_post( $content2 );
					} ?></div>
            </div>
            <h4 class="experience-info wow fadeInRight"><span
                        class="text-primary"><?php if ( isset( $heading{1} ) ) {
						echo wp_kses_post( $heading );
					} ?></span><br> <?php if ( isset( $heading_2{1} ) ) {
					echo wp_kses_post( $heading_2 );
				} ?>
            </h4>
        </div>
    </section>

	<?php
	return ob_get_clean();
}


/*
 * Clients
 */
add_shortcode( 'goarch_clients', 'goarch_clients_func' );
function goarch_clients_func( $atts, $content ) {
	ob_start();
	$content = !empty( $content ) ? $content : "";

	$atts = shortcode_atts(
		array(
			'css' => '',
			'heading' => esc_html__( 'Our', 'goarch' ),
			'heading_2' => esc_html__( 'clients', 'goarch' ),
			'items' => '',
			'b_url' => '',
			'c_url' => '',
			'src' => '',
			'b_text' => esc_html__( 'Work together', 'goarch' ),
			'section_id' => '',

		), $atts
	);

	$items_v = vc_param_group_parse_atts( $atts['items'] );
	extract( $atts );;

	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), '', $atts );

	?>


    <!-- Clients  -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="clients section <?php echo esc_attr( $css_class ); ?>">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title"><?php if ( isset( $heading{1} ) ) {
						;
					}
					echo wp_kses_post( $heading ); ?> <span
                            class="text-primary"><?php if ( isset( $heading_2{1} ) ) {
							echo wp_kses_post( $heading_2 );
						} ?></span></h2>
                <strong class="fade-title-left"><?php if ( isset( $heading_2{1} ) ) {
						;
					}
					echo wp_kses_post( $heading_2 ); ?></strong>
            </header>
            <div class="section-content">
                <ul class="clients-list">
					<?php

					if ( $items_v ) {
						foreach ( $items_v as $item ) {
							$img = wp_get_attachment_image_src( $item['src'], 'full' );
							?>
                            <li class="client">
                                <a href="<?php if ( isset( $item ['c_url']{1} ) ) {
									echo esc_url( $item ['c_url'] );
								} ?>"><img
                                            alt="" src="<?php if ( isset( $img[0]{1} ) ) {
										echo esc_url( $img[0] );
									} ?>"></a>
                            </li>
						<?php }
					} ?>
                </ul>
            </div>

	        <?php if ( isset( $b_text{2} ) ) { ?>
            <div class="section-content">


                <a href="<?php if ( isset( $b_url{1} ) ) {
					echo esc_url( $b_url );
				} ?>"
                   class="btn btn-shadow-2"><?php if ( isset( $b_text{1} ) ) {
						echo wp_kses_post( $b_text );
					} ?> <i
                            class="icon-next"></i></a>
            </div>

       <?php } ?>

        </div>
    </section>


	<?php
	return ob_get_clean();
}

/*
 * Contacts
 */

add_shortcode( 'goarch_contact_section', 'goarch_contact_section_func' );
/**
 * @param $atts
 * @param $content
 *
 * @return string
 */
function goarch_contact_section_func( $atts, $content ) {
	ob_start();

	extract( $atts = shortcode_atts( array(
		'section_id' => '',
		'css' => '',
		'heading' => esc_html__( 'Get ', 'goarch' ),
		'heading_2' => esc_html__( 'in touch ', 'goarch' ),
		'name' => esc_html__( 'Name ', 'goarch' ),
		'phone' => esc_html__( 'Phone ', 'goarch' ),
		'company' => esc_html__( 'Company ', 'goarch' ),
		'email' => esc_html__( 'Email address *', 'goarch' ),
		'sub_btn' => esc_html__( 'Send', 'goarch' ),
		'message' => esc_html__( 'Message*', 'goarch' ),
		'content' => esc_html__( '+7 (212) 654-33-35', 'goarch' ),
		'items' => '',
		'bg_title' => esc_html__( 'contacts', 'goarch' ),

	), $atts ) );

	$items_v = vc_param_group_parse_atts( $atts['items'] );
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
	?>


    <!-- Contacts -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="contacts section <?php echo esc_attr( $css_class ); ?>">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title"><?php if ( isset( $heading{1} ) ) {
						;
					}
					echo wp_kses_post( $heading ); ?> <span class="text-primary"><?php if ( isset( $heading_2{1} ) ) {
							;
						}
						echo wp_kses_post( $heading_2 ); ?></span></h2>
                <strong class="fade-title-right"><?php if ( isset( $bg_title{1} ) ) {
						;
					}
					echo wp_kses_post( $bg_title ); ?></strong>
            </header>
            <div class="section-content">
                <div class="row-base row">
                    <div class="col-address col-base col-md-4">
						<?php

						if ( $items_v ) {
							foreach ( $items_v as $item ) {

								echo wp_kses_post( $item['content'] );

							}

						} ?>
                    </div>
                    <div class="col-base  col-md-8">
                        <form class="js-ajax-form">
                            <div class="row-field row">
                                <div class="col-field col-sm-6 col-md-4">
                                    <div class="form-group">


                                        <input type="text" class="form-control" name="name"
                                               placeholder="<?php if ( isset( $name{1} ) ) {


										       echo esc_attr( $name );  } ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" required
                                               placeholder="<?php if ( isset( $email{1} ) ) {


										       echo esc_attr( $email ) ; } ?>">
                                    </div>

                                </div>
                                <div class="col-field col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <input type="tel" class="form-control" name="phone"
                                               placeholder="<?php if ( isset( $phone{1} ) ) {


										       echo esc_attr( $phone ); } ?>">
                                    </div>
                                    <div class="form-group">
                                        <input  type="text" class="form-control" name="company"
                                               placeholder=" <?php if ( isset( $company{1} ) ) {


										       echo esc_attr( $company );   } ?>">
                                    </div>
                                </div>
                                <div class="col-field col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <textarea name="message" placeholder=" <?php if ( isset( $message{1} ) ) {


                                        echo esc_attr( $message );  } ?>"></textarea>
                                    </div>
                                </div>


                                <div class="col-message col-field col-sm-12">
                                    <div class="form-group">
                                        <div class="success-message"><i
												<?php
												$success_message = get_theme_mod( 'goarch_c_form_s_susses_title', esc_html__( 'Thank you!. Your message is successfully sent...', 'goarch' ) );
												$error_message = get_theme_mod( 'goarch_c_form_s_error_title', esc_html__( 'We\'re sorry, but something went wrong', 'goarch' ) );
												?>


                                                    class="fa fa-check text-primary"></i> <?php if ( isset( $success_message{1} ) ) {
												;
											}
											echo wp_kses_post( $success_message ); ?></div>
                                        <div class="error-message"><?php if ( isset( $error_message{1} ) ) {
												;
											}
											echo wp_kses_post( $error_message ); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-submit text-right">
                                <button type="submit"
                                        class="btn btn-shadow-2 wow swing"><?php if ( isset( $sub_btn{1} ) ) {
										;
									}
									echo esc_attr( $sub_btn ); ?> <i class="icon-next"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<?php
	return ob_get_clean();
}


/****************goarch_page_heading_section*****************/

add_shortcode( 'goarch_page_heading_section', 'goarch_page_heading_section_func' );
/**
 * @param $atts
 * @param $content
 *
 * @return string
 */
function goarch_page_heading_section_func( $atts, $content ) {
	ob_start();

	extract( $atts = shortcode_atts( array(
		'css' => '',
		'heading' => esc_html__( 'About Arch ', 'goarch' ),
		'items' => '',
		'class' => '',
		'section_id' => '',


	), $atts ) );


	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
	?>


    <!-- Contacts -->


    <main <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="<?php echo esc_attr( $css_class ); ?> main main-inner <?php echo esc_attr( $class ); ?> "
            data-stellar-background-ratio="0.6">
        <div class="container">
            <header class="main-header">
                <h1><?php if ( isset( $heading{1} ) ) {
						echo wp_kses_post( $heading );
					} ?></h1>
				<?php
				$items_v = vc_param_group_parse_atts( $atts['items'] );
				if ( $items_v ) {

					?>
                    <div class="project-title-info">
						<?php foreach ( $items_v as $item ) {
							if ( isset( $item ['t']{0} ) || ( isset( $item ['square_metre']{0} ) ) ) {
								?>

                                <div class="project-info-item"><span
                                            class="text-primary"><?php if ( isset( $item ['t_p']{1} ) ) {
											echo wp_kses_post( $item ['t_p'] );
										} ?></span> <?php if ( isset( $item ['t']{1} ) ) {
										echo wp_kses_post( $item ['t'] );
									} ?>
                                    <sup><?php if ( isset( $item ['square_metre']{0} ) ) {
											echo wp_kses_post( $item ['square_metre'] );
										} ?></sup>
                                </div>
								<?php

							}; ?>


						<?php } ?>
                    </div>
				<?php } ?>
            </header>
        </div>

        <!-- Lines -->

        <div class="page-lines">
            <div class="container">
                <div class="col-line col-xs-4">
                    <div class="line"></div>
                </div>
                <div class="col-line col-xs-4">
                    <div class="line"></div>
                </div>
                <div class="col-line col-xs-4">
                    <div class="line"></div>
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </main>
	<?php
	return ob_get_clean();
}


/*
 * goarch_about_section
 */

add_shortcode( 'goarch_about_section', 'goarch_about_section_func' );
/**
 * @param $atts
 * @param $content
 *
 * @return string
 */
function goarch_about_section_func( $atts, $content ) {
	ob_start();

	extract( $atts = shortcode_atts( array(
		'css' => '',
		'section_id' => '',
		'heading' => esc_html__( 'we turn ideas into works of art', 'goarch' ),
		't_p' => esc_html__( '.', 'goarch' ),
		'd' => esc_html__( 'For each project we establish relationships with partners who we know will help us create added value for your project. As well as bringing together the public and private sectors, we make sector-overarching links to gather knowledge and to learn from each other. The way we undertake projects is based on permanently applying values that reinforce each other: socio-cultural value, experiental value, building-technical value and economical value. This way of working allows us to raise your project to a higher level.', 'goarch' ),


	), $atts ) );


	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
	$type = goarch_get_tememe_color();

	$class_se = '';

	if ( $type != 'dark' ) {
		$class_se = "section";
	}

	?>

    <!-- About  -->

    <section <?php if ( isset( $section_id{0} ) ) { ?>
        id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="about<?php echo esc_attr( $css_class ); ?>  <?php echo esc_attr( $class_se ) ?>">
        <div class="container">
            <div class="entry">
                <h3 class="entry-title"><?php if ( isset( $heading{1} ) ) {
						echo wp_kses_post( $heading );
					} ?><span
                            class="text-primary"><?php if ( isset( $t_p{0} ) ) {
							echo wp_kses_post( $t_p );
						} ?></span></h3>
                <div class="entry-text">
					<?php if ( isset( $d{1} ) ) {
						echo wp_kses_post( $d );
					}
					echo do_shortcode($content)
					?>
                </div>
            </div>
        </div>
    </section>

	<?php
	return ob_get_clean();
}

/*
 * Services
 */

add_shortcode( 'goarch_services', 'goarch_services_func' );
/**
 * @param $atts
 * @param $content
 *
 * @return string
 */
function goarch_services_func( $atts, $content ) {
	ob_start();

	extract( $atts = shortcode_atts( array(
		'css' => '',
		'heading' => esc_html__( 'OUR', 'goarch' ),
		't_p' => esc_html__( 'Services', 'goarch' ),
		'items' => '',
		'section_id' => '',

	), $atts ) );

	$items_v = vc_param_group_parse_atts( $atts['items'] );
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
	?>


    <!-- Services -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="services section <?php echo esc_attr( $css_class ); ?>">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title"><span
                            class="text-primary"><?php if ( isset( $heading{1} ) ) {
							echo wp_kses_post( $heading );
						} ?></span> <?php if ( isset( $t_p{1} ) ) {
						echo wp_kses_post( $t_p );
					} ?>
                </h2>
            </header>
            <div class="section-content">
                <div class="row-services row-base row">
					<?php if ( $items_v ) {

						foreach ( $items_v as $item ) {
							$img2 = wp_get_attachment_image_src( $item['img'], 'full' );
							?>

                            <div class="col-base col-service col-sm-6 col-md-4 wow fadeInUp"
                                 data-wow-delay="<?php if ( isset( $item['delay']{1} ) ) {
								     echo wp_kses_post( $item['delay'] );
							     } ?>">
                                <div class="service-item">
									<?php if ( isset( $item['url_l'] ) ) { ?>
                                        <a href="<?php echo esc_url( $item['url_l'] ); ?>">
                                            <img alt="" src="<?php if ( isset( $img2[0]{1} ) ) {
												echo esc_url( $img2[0] );
											} ?>">
                                        </a>
									<?php } else { ?>
                                        <img alt="" src="<?php if ( isset( $img2[0]{1} ) ) {
											echo esc_url( $img2[0] );
										} ?>">
									<?php } ?>

                                    <h4><?php if ( isset( $item['t']{1} ) ) {
											echo wp_kses_post( $item['t'] );
										} ?></h4>
                                    <p><?php if ( isset( $item['content']{1} ) ) {
											echo wp_kses_post( $item['content'] );
										} ?></p>
                                </div>
                            </div>
							<?php
						}

					} ?>

                </div>
            </div>
        </div>
    </section>

	<?php
	return ob_get_clean();
}

/*
 * Objects
 */

add_shortcode( 'goarch_objects', 'goarch_objects_func' );
/**
 * @param $atts
 * @param $content
 *
 * @return string
 */
function goarch_objects_func( $atts, $content ) {
	ob_start();

	extract( $atts = shortcode_atts( array(
		'css' => '',
		'heading' => esc_html__( 'WE ARE ', 'goarch' ),
		't_p' => esc_html__( 'WORLDWIDE', 'goarch' ),
		'src' => '',
		'items' => '',
		'section_id' => '',


	), $atts ) );

	$items_v = vc_param_group_parse_atts( $atts['items'] );
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );


	?>


    <!-- Objects -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="objects section <?php echo esc_attr( $css_class ); ?> ">
        <div class="container">
            <header class="section-header">
                <h2 class="section-title"><?php if ( isset( $heading{1} ) ) {
						echo wp_kses_post( $heading );
					} ?><span
                            class="text-primary"><?php if ( isset( $t_p{1} ) ) {
							echo wp_kses_post( $t_p );
						} ?></span></h2>
            </header>
            <div class="section-content">
                <div class="objects">
					<?php $img = wp_get_attachment_image_src( $src, 'full' ); ?>
                    <img alt="" class="img-responsive" src="<?php if ( isset( $img[0]{1} ) ) {
						echo esc_url( $img[0] );
					} ?>">

                    <!-- Objects -->
					<?php

					if ( $items_v ) {
						foreach ( $items_v as $item ) {
							$class = '';
							if ( ( isset ( $item['style'] ) ) == true ) {
								$class = 'in';
							}

							?>
                            <div class="object-label"
                                 style="left:<?php if ( isset( $item['p_left']{1} ) ) {
								     echo wp_kses_post( $item['p_left'] );
							     } ?>; top:<?php if ( isset( $item['p_top']{1} ) ) {
								     echo wp_kses_post( $item['p_top'] );
							     } ?>;">
                                <div class="object-info <?php echo esc_attr( $class ); ?>   ">
                                    <h3 class="object-title"><?php if ( isset( $item['t']{1} ) ) {
											echo wp_kses_post( $item['t'] );
										} ?></h3>
                                    <div class="object-content">
										<?php if ( isset( $item['content']{1} ) ) {
											echo wp_kses_post( $item['content'] );
										} ?>
                                    </div>
                                </div>
                            </div>
						<?php }
					} ?>

                </div>
            </div>
        </div>
    </section>


	<?php
	return ob_get_clean();
}

/**************** page PROJECTS  shortcodes******************/

/*
* Project
*/


add_shortcode( 'goarch_project_block', 'goarch_project_block_func' );
/**
 * @param $atts
 * @param $content
 *
 * @return string
 */
function goarch_project_block_func( $atts, $content ) {
	ob_start();
	$content = !empty( $content ) ? $content : "";

	$atts = shortcode_atts(
		array(
			'css' => '',
			'heading' => '',
			'posts' => esc_html__( '8', 'goarch' ),
			'order' => 'DESC',
			'orderby' => 'date',
			'projects_category' => 'all',
			'title' => '',
			't_p' => '',
			'btn_text' => esc_html__( 'More projects', 'goarch' ),
			'btn_url' => '',
			'post_id' => '',
			'section_id' => '',
			'type_l' => '1',
			'img_type' => '1'
		), $atts
	);


	extract( $atts );
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );


	$goarch = goarch_get_global_class();
	$goarch_cat = 0;
	$goarch_category = get_category( get_query_var( 'cat' ) );
	if ( isset( $goarch_category->cat_ID ) ) {
		$goarch_cat = $goarch_category->cat_ID;
	} else {
		$goarch_cat = 0;
	}
	//$wp_query = goarch_get_wp_query();

	$myarray = array();

	$myarray = explode( ',', $post_id );


	$args = array(
		'posts_per_page' => $posts,
		'order' => $order,
		'post_status' => 'publish',
		'post_type' => 'projects',
		'cat' => $goarch_cat,
		'taxonomy' => 'projects_categories',
		'meta_query' => array(
			array(
				'key' => '_thumbnail_id'
			)
		)


	);

	if ( $projects_category != 'all' ) {
		$str = $projects_category;
		$arr = explode( ',', $str );

		$args['tax_query'] = array(
			array(
				'taxonomy' => 'projects_categories',
				'terms' => $arr,
				'field' => 'slug'
			)
		);

	}


	if ( isset( $post_id{1} ) ) {
		$args['post__in'] = $myarray;
		$args['orderby'] = 'post__in';

	} else {
		$args['orderby'] = $orderby;
	}


	if ( isset( $_GET['s'] ) && !empty( $_GET['s'] ) ) {
		$args['s'] = sanitize_text_field( $_GET['s'] );
	}

	if ( isset( $wp_query->query['tag'] ) && !empty( $wp_query->query['tag'] ) ) {
		$args['tag'] = ( $wp_query->query['tag'] );
	}


	?>


    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="projects <?php echo esc_attr( $css_class ); ?>">
        <div class="js-projects-gallery">
            <div class=" projects_block row">
				<?php


				$wp_query = new WP_Query( $args );


				$j = 1;
				if ( $wp_query->have_posts() ):
					while ( $wp_query->have_posts() ) {
						$wp_query->the_post();
						$main_class = '';
						if ( $j % 2 == 0 ) {
							$main_class = ' project-light';
						}
						$j ++;

						?>

                        <div
                                class="project project_item <?php echo esc_attr( $main_class ); ?> col-sm-6 col-md-4 col-lg-3">
							<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                            <a
								<?php if ( $type_l == 1 ) { ?>
                                    class="link"
								<?php } ?>
                                    href="<?php if ( $type_l == 1 ) {
										the_permalink();
									} else {
										echo esc_url( $image_url[0] );
									} ?>" title=" <?php echo the_title(); ?>">
                                <figure>
									<?php if ( $img_type == 1 ) { ?>
                                        <img src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ); ?>">
									<?php } else {
										the_post_thumbnail( 'goarch-image-480x880-croped' );
									} ?>

                                    <figcaption>
                                        <h3 class="project-title">
											<?php echo the_title(); ?>
                                        </h3>
                                        <h4 class="project-category">

											<?php $terms = get_the_terms( get_the_ID(), 'projects_categories' );
											if ( $terms ) {
												foreach ( $terms as $term ) {
													?>
													<?php echo wp_kses_post( $term->name ); ?> <br>


													<?php
												}
											}
											?>
                                        </h4>
                                        <div class="project-zoom"></div>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>

						<?php

					}


					wp_reset_postdata();
				endif; ?>

            </div>
        </div>
		<?php if ( isset( $btn_text{1} ) ) {

			?>

            <div class="section-content text-center">
                <a href="<?php echo esc_url( the_permalink() ); ?>"
                   class="btn p-btn btn-gray"><?php if ( isset( $btn_text{1} ) ) {
						echo wp_kses_post( $btn_text );
					} ?></a></div>

		<?php } ?>
    </section>

	<?php


	?>
    <script>


        function initialize_map() {
        }

        jQuery(document).ready(function ($) {


            var total =  <?php echo wp_kses_post( $wp_query->max_num_pages );?>;
            var ajax = true;
            var count = 2;

            $('.p-btn').click(function () {

                jQuery(this).addClass('active2');
                if (ajax) {
                    if (count > total + count) {
                        return false;
                    } else {
                        if ($("div").is(".no_posts_1")) return;
                        loadArticle(count);
                        count++;

                    }
                    ajax = false;
                }
                return false;

            });


            function loadArticle(pageNumber) {

                var ofset = $(".projects_block ").length;
                var posttype = "<?php
					if ( isset( $wp_query->query['post_type'] ) ) {
						echo esc_attr( sanitize_text_field( $wp_query->query['post_type'] ) );
					}
					?>";
                var cat = "<?php
					if ( is_front_page() ) { // is the index page cat = 0
						echo 0;
					} else {
						if ( get_the_category() ) {
							echo wp_kses_post( $goarch_cat );
						}

					} ?>";
                var is_sticky = "";
                var tag = '<?php
					if ( isset( $wp_query->query['tag'] ) && !empty( $wp_query->query['tag'] ) ) {
						echo wp_kses_post( $wp_query->query['tag'] );
					}
					?>';
                var order = '<?php echo esc_attr( $atts['order'] )?>';
                var orderby = '<?php echo esc_attr( $atts['orderby'] )?>';

                jQuery('.p-btn').attr('disabled', true);

                $.ajax({
                    url: "<?php echo esc_url( site_url() ); ?>/wp-admin/admin-ajax.php",
                    type: 'POST',
                    data: "action=goarch_infinite_projects_scroll&page_no=" + pageNumber + "&ofset=" + ofset +
                        "&cat=" + cat + '&tag=' + tag + '&order='+order +'&orderby='+orderby+
                        '&type=<?php echo esc_attr( $type_l );  ?>&img_type=<?php echo esc_attr( $img_type ); ?>' + "&is_sticky=" + is_sticky + "&post_per_page=<?php echo wp_kses_post( $posts );  ?>" + "&term=<?php  echo wp_kses_post( $projects_category ); ?>",
                    success: function (html) {

                        var $moreBlocks = jQuery(html).filter('.project_item');
                        jQuery(".projects_block").append($moreBlocks);


                        ajax = true;

                        jQuery('.p-btn').attr('disabled', false);
                    }
                });
                return false;
            }


        });
    </script>
	<?php
	return ob_get_clean();
}

/*
 * Project - details
 */


add_shortcode( 'goarch_project_details_block', 'goarch_project_details_block_func' );
function goarch_project_details_block_func( $atts, $content ) {
	ob_start();
	$content = !empty( $content ) ? $content : "";

	$atts = shortcode_atts(
		array(

			'items' => '',
			'css' => '',
			'section_id' => '',
			'type_l' => '1'


		), $atts
	);


	extract( $atts );

	$tems_v = vc_param_group_parse_atts( $atts['items'] );
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), '', $atts );

	?>


    <!-- About  -->


    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="project-details <?php echo esc_attr( $css_class ); ?>">
        <div class="container">
			<?php

			if ( isset( $tems_v ) ) {
				foreach ( $tems_v as $item ) {

					$item = shortcode_atts(
						array(
							'class' => '',
							'class2' => '',
							'content' => '',
							'd' => '',
							'images' => '',


						), $item
					);
					extract( $item );

					$img_arr = wp_get_attachment_image_src( $images, 'full' );
					?>
                    <div class="project-details-item">
                        <div class="row">
                            <div class="project-details-info wow <?php echo esc_attr( $class2 ); ?>">
                                <h3 class="project-details-title">

									<?php if ( isset( $content{1} ) ) {
										echo wp_kses_post( $content );
									} ?>
                                </h3>
                                <p class="project-details-descr"><?php if ( isset( $d{1} ) ) {
									echo wp_kses_post( $d );
								} ?></div>
                            <div class="project-details-img col-md-8 <?php echo esc_attr( $class ); ?>">
                                <img alt="" class="img-responsive"
                                     src="<?php if ( isset( $img_arr{1} ) ) {
									     echo esc_url( $img_arr[0] );
								     } ?>">
                            </div>
                        </div>
                    </div>
				<?php }
			} ?>
        </div>
    </section>
	<?php
	return ob_get_clean();
}

/**
 * goarch_map
 */
add_shortcode( 'goarch_map', 'goarch_map_func' );

function goarch_map_func( $atts, $content ) {

	ob_start();

	if ( isset( $atts['images']{0} ) ) {
		$img_arr = wp_get_attachment_image_src( $atts['images'], 'full' );
		if ( isset( $img_arr[0] ) ) {
			$atts['images'] = $img_arr[0];
		}

	}
	$content = !empty( $content ) ? $content : "";
	$atts = shortcode_atts(
		array(


			'd' => '',
			'section_id' => '',
			'lat' => '-37.823534',
			'lng' => '144.975617',
			'zoom' => 16,
			'icon' => '',
			't' => '',
			'css' => '',
			'map_items' => '',
			'map_visibility' => '1',
			'address' => '',
			'logo' => '',
			'img_bg' => '',
			'slogan' => esc_html__( 'contacts', 'goarch' ),


		), $atts
	);


	extract( $atts );
	$img_src = wp_get_attachment_image_src($atts['img_bg'], 'full' );
	$address_items = vc_param_group_parse_atts( $atts['address'] );
	$map_items = vc_param_group_parse_atts( $atts['map_items'] );
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), '', $atts );


	?>


    <!-- Contacts Details -->

    <section <?php if ( isset( $section_id{0} ) ) { ?> id="<?php echo esc_attr( $section_id ); ?>"  <?php } ?>
            class="contact-details <?php echo esc_attr( $css_class ); ?>">
		<?php
		if ( $map_visibility != '2' ) { ?>
            <div class="col-map col-md-7">
                <div id="map" class="gmap map col-md-7" data-lat="<?php if ( isset( $lat{1} ) ) {
					echo esc_attr( $lat );
				} ?>"
                     data-lng="<?php if ( isset( $lng{1} ) ) {
					     echo esc_attr( $lng );
				     } ?>">
					<?php if ( isset( $logo{1} ) ) {
						?>
                        <div class="map-title">

                            <h3><?php echo wp_kses_post( $logo ); ?></h3></div>


						<?php
					} ?>
					<?php
					foreach ( $map_items as $item ) {

						?>
                        <div class="map-address-row">
                            <i class="<?php echo esc_attr( $item['icon'] ); ?>"></i>
                            <span class="text"><?php echo wp_kses_post( $item['content'] ); ?></span>


                        </div>
						<?php
					}
					?>
                </div>
            </div>


		<?php $map_style = get_theme_mod( 'goarch_map_stylemap_json', '[]' );

		if ( strlen( str_replace( ' ', '', $map_style ) ) < 5 ) {
			$map_style = '[]';
		}
		?>
            <script>


                /************/
                function initialize() {
                    mapLocation = new google.maps.LatLng(parseFloat(<?php echo esc_attr( $lat ); ?>), parseFloat(<?php echo esc_attr( $lng ); ?>));

                    var mapOptions = {
                        zoom: <?php echo (int) $zoom; ?>, // Change zoom here
                        center: mapLocation,
                        scrollwheel: false,
                        styles: <?php echo wp_kses_post( $map_style ); ?>
                    };
                    var contentString = '<div class="map-info">' + jQuery("#map").html() + '</div>';

                    map = new google.maps.Map(document.getElementById('map'),
                        mapOptions);


                    //change address details here


                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });


                    marker = new google.maps.Marker({
                        map: map,
                        draggable: true,
                        title: '<?php  echo esc_html( get_bloginfo( 'name' ) ); ?>', //change title here
                        animation: google.maps.Animation.DROP,
                        position: mapLocation
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                        infowindow.open(map, marker);
                    });


                }


            </script>
		<?php }



	?>


        <div class="contact-info col-md-5 col-md-offset-7"
	<?php if(isset($img_src{1})) { ?>
		style="background: url('<?php echo esc_url($img_src[0]); ?>'); !important;" <?php } ?> >


            <div class="contact-info-content">

                <div class="contact-info-title"><?php if ( isset( $slogan{1} ) ) {
						echo esc_attr( $slogan );
					} ?></div>
				<?php if ( $address_items ) {
					foreach ( $address_items as $items ) {
						$items = shortcode_atts(
							array(
								
								'contacts' => '',
								'title' => '',
								'fa' => '',

							), $items
						);
						$contacts_items = vc_param_group_parse_atts( $items['contacts'] );
						extract( $items );




						?>

                    <div class="contact-row">

						<?php if ( isset( $fa ) && $fa != '' ) { ?>
                            <i class="fa <?php echo esc_attr( $fa ); ?>"></i>
						<?php } ?>


                        <div class="contact-body">
						<?php if ( isset( $title{1} ) ) { ?>
                            <h4><?php echo wp_kses_post( $title ); ?></h4>
						<?php } ?>
						<?php if ( $contacts_items ) {
							foreach ( $contacts_items as $c_items ) {
								$c_items = shortcode_atts(
									array(
										'class2' => '',
										'd' => '',


									), $c_items
								);

								extract( $c_items );



								if (isset($d{1}) ) { ?>
                                    <div class="   <?php echo esc_attr($class2); ?>"><?php if (isset($d{1})) {
											echo wp_kses_post($d);
										} ?></div>
								<?php }
							} ?>
                            </div>
                            </div>
							<?php

						}
					}

				} ?>


            </div>
        </div>
    </section>

	<?php

	return ob_get_clean();
}


add_shortcode( 'goarch_map', 'goarch_map_func' );
